const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl : 'https://react-gallery-db1yvbqgp-irfan91catalyze.vercel.app/'
  },
});
